# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the gfeeds package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gfeeds 2.2.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-03-25 14:38+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: ../gfeeds/feeds_view.py:13
msgid "All feeds"
msgstr ""

#: ../gfeeds/feed_item.py:62
#, python-brace-format
msgid "Error: unable to parse datetime {0} for feeditem {1}"
msgstr ""

#: ../gfeeds/feeds_manager.py:83
#, python-brace-format
msgid "Feed {0} exists already, skipping"
msgstr ""

#: ../gfeeds/preferences_window.py:24
msgid "General"
msgstr ""

#: ../gfeeds/preferences_window.py:27
msgid "General preferences"
msgstr ""

#: ../gfeeds/preferences_window.py:29
msgid "Show newer articles first"
msgstr ""

#: ../gfeeds/preferences_window.py:34
msgid "Open links in your browser"
msgstr ""

#: ../gfeeds/preferences_window.py:38
msgid "Use external video player for YouTube"
msgstr ""

#: ../gfeeds/preferences_window.py:40
msgid "Requires youtube-dl and a compatible video player"
msgstr ""

#: ../gfeeds/preferences_window.py:46
msgid "Preferred video player"
msgstr ""

#: ../gfeeds/preferences_window.py:50
msgid "Maximum article age"
msgstr ""

#: ../gfeeds/preferences_window.py:51
msgid "In days"
msgstr ""

#: ../gfeeds/preferences_window.py:58
msgid "Refresh preferences"
msgstr ""

#: ../gfeeds/preferences_window.py:60
msgid "Refresh articles on startup"
msgstr ""

#: ../gfeeds/preferences_window.py:64
msgid "New articles notification"
msgstr ""

#: ../gfeeds/preferences_window.py:68
msgid "Enable auto-refresh"
msgstr ""

#: ../gfeeds/preferences_window.py:72
msgid "Auto-refresh interval"
msgstr ""

#: ../gfeeds/preferences_window.py:73
msgid "In seconds"
msgstr ""

#: ../gfeeds/preferences_window.py:80
msgid "Cache"
msgstr ""

#: ../gfeeds/preferences_window.py:82
msgid "Clear caches"
msgstr ""

#: ../gfeeds/preferences_window.py:83
msgid "Clear"
msgstr ""

#: ../gfeeds/preferences_window.py:109
msgid "Appearance"
msgstr ""

#: ../gfeeds/preferences_window.py:112
msgid "Appearance preferences"
msgstr ""

#: ../gfeeds/preferences_window.py:114
msgid "Dark mode"
msgstr ""

#: ../gfeeds/preferences_window.py:118
msgid "Reader mode theme"
msgstr ""

#: ../gfeeds/preferences_window.py:122
msgid "Automatic"
msgstr ""

#: ../gfeeds/preferences_window.py:122
msgid "Light"
msgstr ""

#: ../gfeeds/preferences_window.py:122
msgid "Dark"
msgstr ""

#: ../gfeeds/preferences_window.py:126
msgid "Show article thumbnails"
msgstr ""

#: ../gfeeds/preferences_window.py:131
msgid "Show full articles titles"
msgstr ""

#: ../gfeeds/preferences_window.py:136
msgid "Show full feeds names"
msgstr ""

#: ../gfeeds/preferences_window.py:143
msgid "Font preferences"
msgstr ""

#: ../gfeeds/preferences_window.py:145
msgid "Use system font for titles"
msgstr ""

#: ../gfeeds/preferences_window.py:149
msgid "Use system font for paragraphs"
msgstr ""

#: ../gfeeds/preferences_window.py:153
msgid "Custom title font"
msgstr ""

#: ../gfeeds/preferences_window.py:157
msgid "Custom paragraph font"
msgstr ""

#: ../gfeeds/preferences_window.py:161
msgid "Custom monospace font"
msgstr ""

#: ../gfeeds/preferences_window.py:173
msgid "Privacy"
msgstr ""

#: ../gfeeds/preferences_window.py:177
msgid "Privacy preferences"
msgstr ""

#: ../gfeeds/preferences_window.py:179
msgid "Enable JavaScript"
msgstr ""

#: ../gfeeds/preferences_window.py:184
msgid "Try to block advertisements"
msgstr ""

#: ../gfeeds/preferences_window.py:186 ../gfeeds/preferences_window.py:221
msgid "Requires app restart"
msgstr ""

#: ../gfeeds/preferences_window.py:190
msgid "Update advertisement blocking list"
msgstr ""

#: ../gfeeds/preferences_window.py:191
msgid "Updates automatically every 10 days"
msgstr ""

#: ../gfeeds/preferences_window.py:192
msgid "Update"
msgstr ""

#: ../gfeeds/preferences_window.py:205
msgid "Advanced"
msgstr ""

#: ../gfeeds/preferences_window.py:208
msgid "Advanced preferences"
msgstr ""

#: ../gfeeds/preferences_window.py:210
msgid "Maximum refresh threads"
msgstr ""

#: ../gfeeds/preferences_window.py:212
msgid "How many threads to use for feeds refresh"
msgstr ""

#: ../gfeeds/preferences_window.py:219
msgid "Experimental GtkListView for articles list"
msgstr ""

#: ../gfeeds/preferences_window.py:227
msgid "Troubleshooting"
msgstr ""

#: ../gfeeds/preferences_window.py:229
msgid "Export Configuration as JSON"
msgstr ""

#: ../gfeeds/preferences_window.py:230
msgid "Attach this when reporting bugs"
msgstr ""

#: ../gfeeds/preferences_window.py:231
msgid "Export"
msgstr ""

#: ../gfeeds/preferences_window.py:244
msgid "Feeds Configuration"
msgstr ""

#: ../gfeeds/preferences_window.py:248
msgid "_Close"
msgstr ""

#: ../gfeeds/preferences_window.py:251
msgid "Cop_y"
msgstr ""

#: ../gfeeds/feed_parser.py:61
#, python-brace-format
msgid "Errors while parsing feed `{0}`, URL: `{1}`"
msgstr ""

#: ../gfeeds/feed_parser.py:75
#, python-brace-format
msgid "Error decoding unicode data from feed `{0}`, URL: `{1}`"
msgstr ""

#: ../gfeeds/feed_parser.py:82
#, python-brace-format
msgid "Error extracting data from feed `{0}`, URL: `{1}`"
msgstr ""

#: ../gfeeds/feed_parser.py:90
#, python-brace-format
msgid "`{0}` may not be an RSS or Atom feed"
msgstr ""

#: ../gfeeds/webview.py:200
msgid "Link copied to clipboard!"
msgstr ""

#: ../gfeeds/webview.py:222
msgid "Feed content not available for this article"
msgstr ""

#: ../gfeeds/main_leaflet.py:114
#, python-brace-format
msgid "{0} new article"
msgid_plural "{0} new articles"
msgstr[0] ""
msgstr[1] ""

#: ../gfeeds/opml_file_chooser.py:7
msgid "XML files"
msgstr ""

#: ../gfeeds/opml_file_chooser.py:14
msgid "Choose an OPML file to import"
msgstr ""

#: ../gfeeds/opml_file_chooser.py:18
msgid "Open"
msgstr ""

#: ../gfeeds/opml_file_chooser.py:19 ../gfeeds/opml_file_chooser.py:31
msgid "Cancel"
msgstr ""

#: ../gfeeds/opml_file_chooser.py:26
msgid "Choose where to save the exported OPML file"
msgstr ""

#: ../gfeeds/opml_file_chooser.py:30
msgid "Save"
msgstr ""

#: ../gfeeds/manage_feeds_window.py:257
msgid "Manage Feeds"
msgstr ""

#: ../gfeeds/manage_feeds_window.py:321
msgid "Do you want to delete these feeds?"
msgstr ""

#: ../gfeeds/manage_feeds_window.py:324 ../gfeeds/__main__.py:239
#: ../gfeeds/__main__.py:273
msgid "_Cancel"
msgstr ""

#: ../gfeeds/manage_feeds_window.py:326
msgid "_Delete"
msgstr ""

#: ../gfeeds/headerbar.py:197
msgid "There were problems with some feeds. Do you want to remove them?"
msgstr ""

#: ../gfeeds/headerbar.py:202
msgid "_Keep"
msgstr ""

#: ../gfeeds/headerbar.py:204
msgid "_Remove"
msgstr ""

#: ../gfeeds/__main__.py:232
msgid "Do you want to import these feeds?"
msgstr ""

#: ../gfeeds/__main__.py:242 ../gfeeds/__main__.py:276
msgid "_Import"
msgstr ""

#: ../gfeeds/__main__.py:269
msgid "Do you want to import this feed?"
msgstr ""

#: ../gfeeds/__main__.py:301
msgid "url"
msgstr ""

#: ../gfeeds/__main__.py:304
msgid "opml file local url or rss remote url to import"
msgstr ""

#: ../gfeeds/util/opml_parser.py:33
msgid "Error: OPML path provided does not exist"
msgstr ""

#: ../gfeeds/util/opml_parser.py:44
#, python-brace-format
msgid "Error parsing OPML file `{0}`"
msgstr ""

#: ../gfeeds/util/build_reader_html.py:104
#, python-brace-format
msgid "Author: <a href=\"{0}\">{1}</a>"
msgstr ""

#: ../gfeeds/util/build_reader_html.py:108
#, python-brace-format
msgid "Author: {0}"
msgstr ""

#: ../gfeeds/util/download_manager.py:112
#, python-brace-format
msgid "`{0}`: connection timed out"
msgstr ""

#: ../gfeeds/util/download_manager.py:121
#, python-brace-format
msgid "`{0}` might not be a valid address"
msgstr ""

#: ../gfeeds/util/download_manager.py:161
#, python-brace-format
msgid "Error downloading `{0}`, code `{1}`"
msgstr ""

#: ../gfeeds/util/get_favicon.py:48
#, python-brace-format
msgid "Error downloading favicon for `{0}`"
msgstr ""

#: ../data/ui/main_leaflet.blp:40
msgid "Offline"
msgstr ""

#: ../data/ui/left_headerbar.blp:6
msgid "_Show Read Articles"
msgstr ""

#: ../data/ui/left_headerbar.blp:7
msgid "Mark All as Read"
msgstr ""

#: ../data/ui/left_headerbar.blp:8
msgid "Mark All as _Unread"
msgstr ""

#: ../data/ui/left_headerbar.blp:11
msgid "Show _Empty Feeds"
msgstr ""

#: ../data/ui/left_headerbar.blp:12
msgid "_Manage Feeds"
msgstr ""

#: ../data/ui/left_headerbar.blp:13
msgid "_Import OPML"
msgstr ""

#: ../data/ui/left_headerbar.blp:14
msgid "E_xport OPML"
msgstr ""

#: ../data/ui/left_headerbar.blp:17
msgid "P_references"
msgstr ""

#: ../data/ui/left_headerbar.blp:18
msgid "_Keyboard Shortcuts"
msgstr ""

#: ../data/ui/left_headerbar.blp:19
msgid "_About Feeds"
msgstr ""

#: ../data/ui/left_headerbar.blp:28
msgid "Filter"
msgstr ""

#: ../data/ui/left_headerbar.blp:32
msgid "Add Feed"
msgstr ""

#: ../data/ui/left_headerbar.blp:36
msgid "Menu"
msgstr ""

#: ../data/ui/left_headerbar.blp:41 ../data/ui/shortcutsWindow.blp:35
msgid "Refresh"
msgstr ""

#: ../data/ui/left_headerbar.blp:46 ../data/ui/shortcutsWindow.blp:39
msgid "Search"
msgstr ""

#: ../data/ui/left_headerbar.blp:50
msgid "There Are Errors"
msgstr ""

#: ../data/ui/sidebar_listbox_row.blp:5
msgid "Mark as Read/Unread"
msgstr ""

#: ../data/ui/sidebar_listbox_row.blp:6 ../data/ui/shortcutsWindow.blp:74
msgid "Open in Browser"
msgstr ""

#: ../data/ui/manage_feeds_headerbar.blp:11
msgid "Manage Tags for Selected Feeds"
msgstr ""

#: ../data/ui/manage_feeds_headerbar.blp:16
msgid "Select/Unselect All"
msgstr ""

#: ../data/ui/manage_feeds_headerbar.blp:21
msgid "Delete Selected Feeds"
msgstr ""

#: ../data/ui/right_headerbar.blp:9
msgid "Open in _Browser"
msgstr ""

#: ../data/ui/right_headerbar.blp:10
msgid "Open in External Media _Player"
msgstr ""

#: ../data/ui/right_headerbar.blp:11
msgid "_Copy Article Link"
msgstr ""

#: ../data/ui/right_headerbar.blp:17 ../data/ui/right_headerbar.blp:96
msgid "View Mode"
msgstr ""

#: ../data/ui/right_headerbar.blp:19
msgid "Web View"
msgstr ""

#: ../data/ui/right_headerbar.blp:24
msgid "Reader Mode"
msgstr ""

#: ../data/ui/right_headerbar.blp:29
msgid "Feed Content"
msgstr ""

#: ../data/ui/right_headerbar.blp:44 ../data/ui/shortcutsWindow.blp:66
msgid "Zoom Out"
msgstr ""

#: ../data/ui/right_headerbar.blp:51 ../data/ui/shortcutsWindow.blp:70
msgid "Reset Zoom"
msgstr ""

#: ../data/ui/right_headerbar.blp:57 ../data/ui/shortcutsWindow.blp:62
msgid "Zoom In"
msgstr ""

#: ../data/ui/right_headerbar.blp:90
msgid "Back to Articles"
msgstr ""

#: ../data/ui/empty_state.blp:7
msgid "Let's get started"
msgstr ""

#: ../data/ui/empty_state.blp:22
msgid "Add new feeds via URL"
msgstr ""

#: ../data/ui/empty_state.blp:27
msgid "Import an OPML file"
msgstr ""

#: ../data/ui/shortcutsWindow.blp:12
msgid "Open Keyboard Shortcuts"
msgstr ""

#: ../data/ui/shortcutsWindow.blp:16
msgid "Open Menu"
msgstr ""

#: ../data/ui/shortcutsWindow.blp:20
msgid "Open Preferences"
msgstr ""

#: ../data/ui/shortcutsWindow.blp:24
msgid "Open Filter"
msgstr ""

#: ../data/ui/shortcutsWindow.blp:28
msgid "Quit"
msgstr ""

#: ../data/ui/shortcutsWindow.blp:32
msgid "Article List"
msgstr ""

#: ../data/ui/shortcutsWindow.blp:43
msgid "Next"
msgstr ""

#: ../data/ui/shortcutsWindow.blp:47
msgid "Previous"
msgstr ""

#: ../data/ui/shortcutsWindow.blp:51
msgid "Show/Hide Read Articles"
msgstr ""

#: ../data/ui/shortcutsWindow.blp:55
msgid "Mark All Read"
msgstr ""

#: ../data/ui/shortcutsWindow.blp:59
msgid "Article"
msgstr ""

#: ../data/ui/add_feed_popover.blp:11
msgid "Enter feed address to add"
msgstr ""

#: ../data/ui/add_feed_popover.blp:26
msgid "Add"
msgstr ""

#: ../data/ui/add_feed_popover.blp:32
msgid "You're already subscribed to that feed!"
msgstr ""

#: ../data/ui/manage_tags_content.blp:17
msgid "New tag name…"
msgstr ""

#: ../data/ui/manage_tags_content.blp:24
msgid "Add Tag"
msgstr ""

#: ../data/ui/manage_tags_content.blp:39
msgid "There are no tags yet"
msgstr ""

#: ../data/ui/manage_tags_content.blp:40
msgid "Add some using the entry above"
msgstr ""

#: ../data/ui/webview.blp:12
msgid "Select an article"
msgstr ""

#: ../data/ui/aboutdialog.ui.in:10
msgid "translator-credits"
msgstr ""

#: ../data/org.gabmus.gfeeds.desktop.in:6
msgid "@prettyname@"
msgstr ""

#: ../data/org.gabmus.gfeeds.desktop.in:7
msgid "News reader for GNOME"
msgstr ""

#: ../data/org.gabmus.gfeeds.desktop.in:16
msgid "rss;reader;feed;news;"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:4
msgid "Feeds"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:15
msgid ""
"Feeds is a minimal RSS/Atom feed reader built with speed and simplicity in "
"mind."
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:16
msgid ""
"It offers a simple user interface that only shows the latest news from your "
"subscriptions."
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:17
msgid ""
"Articles are shown in a web view by default, with javascript disabled for a "
"faster and less intrusive user experience. There's also a reader mode "
"included, built from the one GNOME Web/Epiphany uses."
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:18
msgid "Feeds can be imported and exported via OPML."
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:59
msgid "Article author in reader mode"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:60
msgid "Updated WebKit to API version 6.0"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:68
msgid "Improved configuration management"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:69
msgid "Refactoring around GObject property binding"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:70
msgid "Removed top/bottom margin in reader mode"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:71
msgid "Catch text decoding errors"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:79
msgid "Fix divide by zero issue when adding the first feed"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:87
msgid "Reader mode color scheme can adapt to system theme"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:88
msgid "Fix high CPU usage in web view"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:89
msgid "New message dialog"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:90
msgid "Can open articles in browser from right click menu"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:91
msgid "Added a progress bar to show loading state"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:92
msgid "Small UI improvements"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:93
msgid "Fixes for thumbnail extraction"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:94
msgid "Fix OPML export edge cases"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:95
#: ../data/org.gabmus.gfeeds.appdata.xml.in:245
msgid "Various improvements and bug fixes"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:103
msgid "Links in the web view show a preview of the destination URL"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:104
msgid "Stability improvements related to article thumbnails"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:105
msgid "Reworked dialog windows"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:106
msgid "Fixes for non-unicode character encodings"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:107
msgid "Fixes for importing OPML"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:108
msgid "Various bug fixes and improvements"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:109
msgid "Code refactoring"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:117
#: ../data/org.gabmus.gfeeds.appdata.xml.in:125
msgid "Flatpak dependency fixes"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:133
msgid "Brand new look powered by GTK4 and Libadwaita"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:134
msgid "Articles can have thumbnails"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:135
msgid "New advertisement blocking feature"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:136
msgid "Human friendly article date and time"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:137
msgid "Less intrusive feed error notification"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:138
msgid "Customizable reader mode fonts"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:139
msgid "Empty feeds can be hidden"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:140
msgid "New lightning fast C++ powered backend for feed parsing"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:141
msgid "Experimental article list implementation using GtkListView"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:142
msgid "Removed saved articles feature"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:150
msgid "Fixed view mode button"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:151
msgid "Option to open YouTube links with an external media player"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:152
msgid ""
"Saved articles are no longer stored inside the cache directory, avoiding "
"accidental deletion"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:160
msgid "Feed icons improvements for high DPI screens"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:168
msgid "UI improvements on phones"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:176
msgid "Switched to the new libhandy Avatar widget"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:177
msgid "Added Dutch translation courtesy of Heimen Stoffels"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:184
msgid "Fixed searchbar"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:192
msgid ""
"New tags feature! You can now categorize your feeds by creating custom tags"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:193
msgid "RSS links can now be opened directly in Feeds to be imported"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:194
msgid "Mark as read now only marks the currently visible feeds"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:195
msgid "Added support for the latest version of libhandy"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:196
msgid "Improved message dialogs"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:197
msgid "Performance improvements when importing OPML files"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:198
msgid "Removed option to disable client side decoration"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:205
msgid ""
"Don't remove feeds automatically if they have errors, instead ask the user "
"what to do"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:206
#: ../data/org.gabmus.gfeeds.appdata.xml.in:228
msgid "Updated Italian translation"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:213
msgid "Updated French translation"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:220
msgid "Bug fixes"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:227
msgid "Updated version number"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:235
msgid "You can now add some feeds by just using the website URL"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:236
msgid "New generated placeholder icon for feeds without one"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:237
msgid "Improved reader mode"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:238
msgid "Swipe left on your touchscreen to move back from an article"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:239
msgid "Code blocks in articles now have syntax highlighting"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:240
msgid "Added French translation"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:241
msgid "Removed the colored border left of the articles"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:242
msgid "More keyboard shortcuts"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:243
msgid "Added option to refresh articles on startup"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:244
msgid "Updated to the GNOME 3.36 runtime"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:252
msgid "Added loading progress bar"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:253
msgid "Added Spanish translation"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:254
#: ../data/org.gabmus.gfeeds.appdata.xml.in:265
#: ../data/org.gabmus.gfeeds.appdata.xml.in:279
msgid "Various UI improvements"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:255
#: ../data/org.gabmus.gfeeds.appdata.xml.in:266
msgid "Performance improvements"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:256
#: ../data/org.gabmus.gfeeds.appdata.xml.in:267
#: ../data/org.gabmus.gfeeds.appdata.xml.in:280
#: ../data/org.gabmus.gfeeds.appdata.xml.in:290
#: ../data/org.gabmus.gfeeds.appdata.xml.in:305
#: ../data/org.gabmus.gfeeds.appdata.xml.in:316
#: ../data/org.gabmus.gfeeds.appdata.xml.in:369
msgid "Various bug fixes"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:263
msgid "Load cached articles on startup"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:264
msgid "Added new view mode menu"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:268
msgid "Added Brazilian Portuguese translation"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:269
msgid "Added Russian translation"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:276
msgid "Option to ellipsize article titles for a more compact view"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:277
msgid "Added a search function"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:278
msgid "Updated dependencies"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:287
msgid "Errors with feeds are now shown in the UI"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:288
msgid "Big UI overhaul"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:289
msgid "Updated translations"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:297
msgid "OPML file association"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:298
msgid "Changed left headerbar button order"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:299
msgid "Optimization for updating feeds"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:300
msgid "Redesigned right click/longpress menu"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:301
msgid "Option to show/hide read articles"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:302
msgid "Reworked suggestion bar"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:303
msgid "Changed name to Feeds"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:304
msgid "Improved CPU utilization"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:312
msgid "New right click or longpress menu for articles"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:313
msgid "You can now save articles offline"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:314
msgid "Initial suggestion to add feeds is now less intrusive"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:315
msgid "Read articles are now greyed out"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:323
msgid "Concurrent feeds refresh, with customizable thread count"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:324
msgid "Added German translation (thanks @Etamuk)"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:331
msgid "Fix bugs in reader mode"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:338
msgid "Minor bug fix"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:345
msgid "Improved date and time parsing and display"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:346
msgid "Reader mode can now work on websites without an article tag"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:347
msgid "Slight improvements to the icon"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:348
msgid "New feature to filter articles by feed"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:355
msgid "Improved favicon download"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:356
msgid "Fixed refresh duplicating articles"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:363
msgid "Added option to disable client side decoration"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:364
msgid "Brought primary menu in line with GNOME HIG"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:365
msgid "Added placeholder icon for feeds without an icon"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:366
msgid "Migrated some widgets to Glade templates"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:367
msgid "Option to use reader mode by default"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:368
msgid "Option to show article content from the feed"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:376
msgid "Fixed labels alignment"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:377
msgid "Changed app name to Feeds"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:378
msgid "Added separators for the two sections of the app"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:379
msgid "Using links as feed titles when there is no title"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:380
msgid "Added preference for maximum article age"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:381
msgid "Added zoom keyboard shortcuts"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:382
msgid "Added preference to enable JavaScript"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:383
msgid "Fix window control positions when they are on the left"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:390
msgid "Feeds for websites without favicons will now work"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:397
msgid "Fixed bug with adding new feeds"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:404
msgid "Switched to native file chooser"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:405
msgid "Added empty state initial screen for sidebar"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:406
msgid "Added italian translation"
msgstr ""

#: ../data/org.gabmus.gfeeds.appdata.xml.in:413
msgid "First release"
msgstr ""
